import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { DieEngine } from './dice-tools';
import { QRNGManager } from './QRNG-manager';
import { MDBContainer, MDBRow, MDBCol, MDBInput, Progress, MDBBtn, MDBAlert, Table, TableBody, TableHead }  from 'mdbreact';
import { Select } from './components/select';
import { ResultChart } from './components/results-chart';

const DEFAULT_DISPLAY_TEXT = `Scrivi l'espressione, quindi premi Lancia per lanciare i dadi, oppure Prova per lanciarla più volte per stabilirne la media empiricamente.`;

class App extends Component
{
	constructor()
	{
		super();
		this.state = {
			dieEngine : new DieEngine(),
			qrngManager : new QRNGManager(),
			hand : null,
			display : DEFAULT_DISPLAY_TEXT,
			requestStatus : { current : 0, max : 0 },
			iterations : 100,
			results : {
				labels : [],
				values : [],
				datasetLabel : ''
			}
		};

		this.handleInput = this.handleInput.bind(this);
		this.handleRollButtonClick = this.handleRollButtonClick.bind(this);
		this.handleTestAverageClick = this.handleTestAverageClick.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleIterationsChange = this.handleIterationsChange.bind(this);
	}

	render()
	{
		const { current, max } = this.state.requestStatus;

		const iterationOptions = [
			{ value : 10, label : '10 lanci'},
			{ value : 100, label : '100 lanci'},
			{ value : 500, label : '500 lanci'},
			{ value : 1000, label : '1000 lanci'},
			{ value : 5000, label : '5000 lanci'}
		];

		const { labels, values, datasetLabel } = this.state.results;

		return (
			<MDBContainer>
				<MDBRow>
					<MDBCol sm="12">
						<MDBInput
							type="textarea"
							label="Espressione"
							rows="5"
							onInput={e => this.handleInput(e.target.value)}
							onKeyUp={this.handleKeyUp}
						></MDBInput>
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol sm="12">
						<Progress color="warning" value={current} max={ max } />
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol sm="12">
						<MDBBtn color="primary" onClick={this.handleRollButtonClick}>Lancia</MDBBtn>
						<MDBBtn color="secondary" onClick={this.handleTestAverageClick}>Prova</MDBBtn>
						<Select value={this.state.iterations} onChange={this.handleIterationsChange} options={iterationOptions} ></Select>
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol sm="12">
						<MDBAlert color="primary">{ this.state.display }</MDBAlert>
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol sm="12">
						<ResultChart
							datasetLabel={datasetLabel}
							labels={labels}
							results={values}
						/>
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol sm="12">
						<Table>
							<TableHead>
								<tr>
									<th>Esempio</th>
									<th>Formula</th>
								</tr>
							</TableHead>
							<TableBody>
								<tr>
									<td>Un d6</td>
									<td>1d6</td>
								</tr>
								<tr>
									<td>Un d6 con +3 di bonus</td>
									<td>1d6 + 3</td>
								</tr>
								<tr>
									<td>Vantaggio</td>
									<td>max(1d20, 1d20)</td>
								</tr>
								<tr>
									<td>Svantaggio</td>
									<td>min(1d20, 1d20)</td>
								</tr>
								<tr>
									<td>Esempio di attacco con vantaggio</td>
									<td>max(1d20, 1d20) +5 >= 12 ? 1d12 + 3 : 0 </td>
								</tr>
								<tr>
									<td>Valutazione del critico</td>
									<td>1d20 == 20 ? 4d6 : 2d6</td>
								</tr>
							</TableBody>
						</Table>
					</MDBCol>
				</MDBRow>
			</MDBContainer>

		);
	}

	handleKeyUp(e)
	{
	 	if (event.key === 'Enter')
	 		this.roll();

	}

	handleInput( input )
	{
		let hand = null;
		let display = DEFAULT_DISPLAY_TEXT;
		try{
			hand = this.state.dieEngine.parseHand( input )
		}
		catch( e ){

		}

		this.setState({ hand, display });
	}

	handleIterationsChange(e)
	{
		const iterations = parseInt(e.target.value);
		this.setState({ iterations });
	}

	handleRollButtonClick()
	{
		this.roll();
	}

	handleTestAverageClick()
	{
		if(this.state.hand)
		{

			const app = this;
			const { hand, qrngManager, iterations } = app.state;

			const requestedNumbers = iterations * hand.diceQuantity;

			app.setState({ display : `Caricamento...` });

			const updateProgress = (current, max) => app.setState( { requestStatus : { current, max } } );

			const numbers = qrngManager.getNumbers(requestedNumbers, updateProgress)
				.then( numbers => {

					updateProgress( 0, 0 );

					let total = 0;
					const valuesObtained = [];

					for(let i = 0; i < iterations; i += 1)
					{
						const pregeneratedFactors = numbers.splice(hand.diceQuantity);
						const result = hand.roll( pregeneratedFactors );

						if(!valuesObtained[result])
							valuesObtained[result] = 0;

						valuesObtained[result] += 1;

						total += result;
					}

					const display = `Ottenuta media di ${ total / iterations } con ${iterations} lanci fatti.`;
					const results = {
						datasetLabel : hand.toString(),
						labels : [],
						values : []
					}

					for(var key in valuesObtained)
					{
						results.labels.push(key);
						results.values.push(valuesObtained[key])
					}

					app.setState({ display, results });
				});
		}

	}

	roll()
	{
		if(this.state.hand)
		{
			const hand = this.state.hand;
			this.setState({ display : `Ottenuto ${hand.roll()}` });
		}
	}

}

ReactDOM.render(<App />, document.getElementById('app'));
