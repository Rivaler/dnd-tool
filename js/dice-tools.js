export class DieEngine
{

	parseDice( diceStr )
	{

		// Check if a string was passed, won't work with anything else
		if( typeof diceStr !== 'string')
				throw new Error('Cannot parse: expected a string');

		// Normalize the string for following operations
		diceStr = diceStr.replace(/\s/g, '');
		const diceRegexp = /\b(\d*)d(100|20|12|10|8|6|4|3|2)\b/i;
		const res = diceRegexp.exec(diceStr);
		if(!res)
			throw new Error('Cannot parse: bad format');
		const diceNumber = res[1] || 1;
		const dieFaces = res[2];

		return new DiceCollection(parseInt(dieFaces), parseInt(diceNumber));

	}

	parseHand( exp )
	{
		// Check if a string was passed, won't work with anything else
		if( typeof exp !== 'string')
			throw new Error('Cannot parse: expected a string');

		// TODO - Check that only valid characters are used in the expression
		exp = exp.replace(/min/g, 'Math.min');
		exp = exp.replace(/max/g, 'Math.max');

		let total = 0;
		const diceRegexp = /\b(\d*)\s*d\s*(100|20|12|10|8|6|4|3|2)\b/gi;
		const substitutionArray = [];	// Holds index : numbers pair for substitution
		let dieInfo = diceRegexp.exec(exp);
		while(dieInfo)
		{

			const dieExp = dieInfo[0];

			// Calculate the roll
			const dice = this.parseDice(dieExp);

			substitutionArray.push({
				index : dieInfo.index,
				original : dieExp,
				dice : dice
			});

			// Get next dieInfo, if any
			dieInfo = diceRegexp.exec(exp);
		}

		return new Hand(exp, substitutionArray);
	}

}

export class DiceCollection
{

	constructor( faces, quantity = 1 )
	{
		if(typeof faces !== 'number')
			throw new Error('Invalid faces: must be a number');

		if(typeof quantity !== 'number')
			throw new Error('Invalid quantity: must be a number');

		this.faces = faces;
		this.quantity = quantity >= 0 ? quantity : 0;
		this.average = ((1 + this.faces) / 2) * this.quantity;
	}
	toString()
	{
		return `${this.quantity}d${this.faces}`;
	}

	roll( pregeneratedFactors = [] )
	{

		let total = 0;

		for(let i = 0; i < this.quantity; i += 1)
		{
			const factor = pregeneratedFactors[i] ? pregeneratedFactors[i] : Math.random();
			total += 1 + Math.floor(factor * this.faces);
		}

		return total;
	}

}

export class Hand
{
	constructor(exp, substitutionDice)
	{
		this.exp = exp;
		this.substitutionDice = substitutionDice;
		this.diceQuantity = substitutionDice.map( sDie => sDie.dice ).reduce( ( carry, die ) => carry + die.quantity, 0);

		const substitutionArray = this.substitutionDice.slice().reverse();
		let avgExp = this.exp;
		for(let sub of substitutionArray)
			avgExp = avgExp.substr(0, sub.index) + sub.dice.average + avgExp.substr(sub.index + sub.original.length);

		this.average = eval(avgExp);
	}

	toString()
	{
		return this.exp;
	}

	roll( pregeneratedFactors = [] )
	{
		const substitutionArray = this.substitutionDice.slice().reverse();
		let exp = this.exp;
		for(let sub of substitutionArray)
		{
			if(pregeneratedFactors.length >= sub.dice.quantity)
				exp = exp.substr(0, sub.index) + sub.dice.roll() + exp.substr(sub.index + sub.original.length);
			else
				exp = exp.substr(0, sub.index) + sub.dice.roll( pregeneratedFactors.splice( 0, sub.dice.quantity ) ) + exp.substr(sub.index + sub.original.length);
		}

		return eval(exp);
	}

}