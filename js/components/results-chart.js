import React from "react";
import { Line } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

const dataDefaults = {
    labels: [],
    datasets: [
        {
            label: '',
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(66,133,244,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(66,133,244,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(66,133,244,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        }
    ]
}

const options = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero : true,
                suggestedMin: 0,
                suggestedMax: 100
            }
        }]
    }
};

export const ResultChart = ({labels = [], results = [], datasetLabel = ''}) => {

    let data = Object.assign({}, dataDefaults);
    data.labels = labels;
    data.datasets[0].label = datasetLabel;
    data.datasets[0].data = results;

    return(
        <Line data={data} options={options} />
    );
}
