import React, { Component } from 'react';

const DEFAULTS = {
    options : []
};

export class Select extends Component
{
    constructor(props)
    {
        super(props);

        const { options } = props;

        this.state = Object.assign({}, DEFAULTS, { options });
    }

    render()
    {
        const options = this.state.options.map( (option, index) => <option key={index} value={option.value}>{option.label}</option>);
        const style = {
            display : 'inline-block',
            width : 'auto'
        };

        return(
            <select {...this.props} style={ style } className="browser-default custom-select d-inline-block" >
                {options}
            </select>
        );
    }
}
