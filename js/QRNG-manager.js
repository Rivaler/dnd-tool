import axios from 'axios';

export const QRNG_API_URL = "https://qrng.anu.edu.au/API/jsonI.php";
export const QRNG_REQUEST_LIMIT = 1024;
export const QRNG_BLOCK_SIZE_LIMIT = 1024;
export const QRNG_REQUEST_TYPE_UINT8 = "uint8";
export const QRNG_REQUEST_TYPE_UINT16 = "uint16";
export const QRNG_REQUEST_TYPE_HEX16 = "hex16";
export const QRNG_REQUESTS_PER_SECOND = 10;

const truncateDecimal = ( number ) => {
	return Math.floor( number * 100 ) / 100;
}

const normalizeRandomNumbers = ( numbers, type = QRNG_REQUEST_TYPE_UINT8 ) =>
{
	switch(type)
	{
		case QRNG_REQUEST_TYPE_UINT8 :
			return numbers.map( n => truncateDecimal( n / 255 ) );

		case QRNG_REQUEST_TYPE_UINT16 :
			return numbers.map( n => truncateDecimal( n / 65535 ) );
	}

	throw new Error(`Couldn't normalize type ${type}`);
}

const requestRandomNumbers = (length = QRNG_REQUEST_LIMIT, type = QRNG_REQUEST_TYPE_UINT8, size = QRNG_BLOCK_SIZE_LIMIT) =>
{
	// Returns a promise
	return axios.get(QRNG_API_URL, { params : { length, type, size } });
}

export class QRNGManager
{
	constructor()
	{
		// TODO - Should be different per type. Currently only handles uint8 only
		this.storedNumbers = [];
	}

	fetchNumbers( quantity, progressCallback = false )
	{
		
		const manager = this;

		return new Promise( ( resolve, reject ) => {

			if(manager.storedNumbers.length >= quantity)
			{
				resolve( manager.storedNumbers.splice(0, quantity) );
			}
			else
			{
				requestRandomNumbers()
					.then( ({ data : { data, length, success, error } }) => {

						if(success)
						{
							manager.storedNumbers = [...manager.storedNumbers, ...data];

							if( progressCallback )
								progressCallback( manager.storedNumbers.length, quantity );

							manager.fetchNumbers( quantity, progressCallback ).then( resolve );
						}
						else
						{
							reject(error);
						}

					} )
					.catch( error => {
						reject( error );
					});		
			}

		} );

	}

	getNumbers( quantity, progressCallback = false, type = QRNG_REQUEST_TYPE_UINT8, normalize = true )
	{

		const manager = this;

		// Returns a promise
		return new Promise( ( resolve, reject ) => {

			manager.fetchNumbers( quantity, progressCallback )
				.then( numbers => {

					if(normalize)
						numbers = normalizeRandomNumbers( numbers, type );

					resolve( numbers );

				})
				.catch( reject );

		} );

	}
}